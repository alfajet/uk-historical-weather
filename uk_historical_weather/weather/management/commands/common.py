from ...models import Metric, Location


class GetForeignObjectsMixin():
    def _get_foreign_objects(self, location: str, metric: str):
        try:
            location_obj = Location.objects.get(name=location)
            metric_obj = Metric.objects.get(name=metric)
        except Location.DoesNotExist:
            raise ValueError(f'Location {location} is not registered. Import aborted.')
        except Metric.DoesNotExist:
            raise ValueError(f'Metric {metric} is not registered. Import aborted.')
        else:
            self.location = location_obj
            self.metric = metric_obj
