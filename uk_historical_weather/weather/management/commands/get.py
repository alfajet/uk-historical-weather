import json

from django.core.management.base import BaseCommand

from .common import GetForeignObjectsMixin
from ...models import Value
from ...serializers import ValuePartialSerializer


class Command(GetForeignObjectsMixin, BaseCommand):
    help = "Extract of values for a given location and metric into a json format."

    def add_arguments(self, parser) -> None:
        parser.add_argument('-l', '--location', type=str, help='Location to extract')
        parser.add_argument('-m', '--metric', type=str, help='Metric to extract')
        parser.add_argument('-p', '--path', type=str,
                            help='output file path, if not specified, output to standard output.')
        parser.add_argument('--max', type=int,
                            help="If specified, truncate the output list to the given maximum number.")

    def _get_values(self):
        values = Value.objects.filter(location=self.location, metric=self.metric).order_by('date')
        if self.max:
            if self.max > 0:
                values = values[:self.max]
        data = []
        for v in values:
            serializer = ValuePartialSerializer(instance=v)
            data.append(serializer.data)
        self.data = json.dumps(data, indent=4) if len(data) else None

    def _output_values(self, file_output: bool):
        if self.data:
            if file_output:
                with open(self.path, 'w') as f:
                    f.write(self.data)
                    f.write('\n')
            else:
                self.stdout.write(self.data)

    def handle(self, *args, **options):
        location = options.pop('location', '')
        metric = options.pop('metric', '')
        self.path = options.pop('path', None)
        self.max = options.pop('max', 0)

        try:
            self._get_foreign_objects(location, metric)
            self._get_values()
            self._output_values(bool(self.path))
        except (ValueError, ) as e:
            self.stdout.write(self.style.ERROR(e))
