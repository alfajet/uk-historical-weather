import datetime

import requests
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from tqdm import tqdm

from .common import GetForeignObjectsMixin
from ...models import Value


class Command(GetForeignObjectsMixin, BaseCommand):
    base_url = getattr(settings, 'SOURCE_DATA_BASE_URL') + '{metric}-{location}.json'

    def add_arguments(self, parser):
        parser.add_argument('-l', '--location', type=str, help='Location to fetch')
        parser.add_argument('-m', '--metric', type=str, help='Metric to fetch')

    def _get_remote_data(self):
        url = self.base_url.format(metric=str(self.metric), location=str(self.location))
        r = requests.get(url)
        r.raise_for_status()
        self.data = r.json()

    def _import_data(self):
        data_len = len(self.data)
        skipped_values = 0
        self.stdout.write(f'Importing {data_len} values.')

        progress_bar = tqdm(desc="Importing", total=data_len)
        for idx, value in enumerate(self.data):
            try:
                date = datetime.date(int(value['year']), int(value['month']), 1)
                value = float(value['value'])
                Value.objects.create(date=date, value=value, location=self.location, metric=self.metric)
            except Exception as e:
                self.stderr.write(
                    self.style.WARNING(f'\rERROR: Element {idx + 1} not imported. Error {e}\n'))
                skipped_values += 1
            finally:
                progress_bar.update(1)
        progress_bar.close()
        return skipped_values

    def handle(self, *args, **options):
        location = options.pop('location', '')
        metric = options.pop('metric', '')
        try:
            self._get_foreign_objects(location, metric)
            self._get_remote_data()
            skipped_values = self._import_data()
        except (ValueError, requests.exceptions.RequestException) as e:
            self.stderr.write(self.style.ERROR(str(e)))
            # Raising this exception ensures that the script returns exit code 1. The value can be changed
            # by passing returncode argument to the exception.
            raise CommandError()
        else:
            if skipped_values:
                self.stderr.write(self.style.WARNING(f'{skipped_values} value(s) ignored during the import.'))
            self.stdout.write(self.style.SUCCESS('Import complete.'))
