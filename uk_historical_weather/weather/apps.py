from django.apps import AppConfig


class WeatherConfig(AppConfig):
    name = 'uk_historical_weather.weather'
