from rest_framework import serializers

from .models import Value


class DatePartToIntField(serializers.DateField):
    def to_representation(self, value):
        str_value = super().to_representation(value)
        return int(str_value)


class ValueSerializer(serializers.ModelSerializer):
    month = DatePartToIntField(source='date', format='%m', read_only=True)
    year = DatePartToIntField(source='date', format='%Y', read_only=True)
    location = serializers.StringRelatedField(many=False)
    metric = serializers.StringRelatedField(many=False)

    class Meta:
        model = Value
        fields = 'year month value metric location'.split(' ')


class ValuePartialSerializer(serializers.ModelSerializer):
    month = DatePartToIntField(source='date', format='%m', read_only=True)
    year = DatePartToIntField(source='date', format='%Y', read_only=True)

    class Meta:
        model = Value
        fields = 'value year month'.split(' ')


class ValuePostSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=['%Y-%m', ])

    class Meta:
        model = Value
        fields = 'metric location date value'.split(' ')
