from io import StringIO

import pytest
import responses
from django.core.management import call_command, CommandError

from uk_historical_weather.weather.models import Value


pytestmark = pytest.mark.django_db

BASE_URL = 'https://some.site.tld/somedir/'


@pytest.fixture(autouse=True)
def use_dummy_source_url(settings):
    settings.SOURCE_DATA_BASE_URL = BASE_URL


def get_value(value, year, month):
    return {'value': value, 'year': year, 'month': month}


@pytest.fixture()
def default_command_options():
    yield {'location': 'Scotland', 'metric': 'Tmax'}


@pytest.fixture()
def valid_response():
    valid_value_series = [
        get_value(1.5, 2021, 1),
        get_value(-3, 2021, 2),
        get_value(0, 2021, 3)
    ]
    responses.add(
        responses.GET,
        url=f"{BASE_URL}Tmax-Scotland.json",
        json=valid_value_series
    )


@pytest.fixture()
def response_with_duplicates():
    valid_value_series = [
        get_value(1.5, 2021, 1),
        get_value(1.5, 2021, 1),
    ]
    responses.add(
        responses.GET,
        url=f"{BASE_URL}Tmax-UK.json",
        json=valid_value_series
    )


@pytest.fixture()
def error_response():
    responses.add(
        responses.GET,
        url=f"{BASE_URL}Tmax-England.json",
        body='Error',
        status=500
    )


@responses.activate
def test_fetch_data_check_correctly_imported_3_values(default_command_options, valid_response):
    values_qs = Value.objects.filter(date__year=2021)
    assert len(values_qs) == 0

    call_command('fetch', **default_command_options)

    values_qs = Value.objects.filter(date__year=2021)
    assert len(values_qs) == 3


@responses.activate
def test_fetch_data_check_correctly_added_fk_to_expected_location_and_metric(default_command_options, valid_response):
    call_command('fetch', **default_command_options)

    values_qs = Value.objects.filter(date__year=2021)
    for value in values_qs:
        assert str(value.metric) == 'Tmax'
        assert str(value.location) == 'Scotland'


@responses.activate
def test_fetch_data_with_http_error_check_error_raised(error_response):
    with pytest.raises(CommandError):
        call_command('fetch', **{'location': 'England', 'metric': 'Tmax'})


@responses.activate
def test_fetch_data_with_http_error_check_error_in_std_err(error_response):
    stderr = StringIO()
    with pytest.raises(CommandError):
        call_command('fetch', **{'location': 'England', 'metric': 'Tmax'}, stderr=stderr)
    assert "500 Server Error" in stderr.getvalue()


@responses.activate
def test_fetch_data_having_duplicate_check_error_in_std_error(response_with_duplicates):
    stderr = StringIO()
    call_command('fetch', **{'location': 'UK', 'metric': 'Tmax'}, stderr=stderr)
    print(stderr.getvalue())
    stderr_output = stderr.getvalue()
    assert "Element 2 not imported. Error duplicate key value" in stderr_output
    assert "1 value(s) ignored during the import." in stderr_output
