import datetime
import random
from collections import namedtuple
from typing import List

import pytest

from uk_historical_weather.weather.models import Metric, Location, Value

pytestmark = pytest.mark.django_db

TestMetrics = namedtuple('TestMetrics', 'Tmax rainfall')
TestLocations = namedtuple('TestLocations', 'England Scotland')


@pytest.fixture
def test_metrics() -> TestMetrics:
    yield TestMetrics(
        Metric.objects.get(name='Tmax'),
        Metric.objects.get(name='Rainfall')
    )


@pytest.fixture
def test_locations() -> TestLocations:
    yield TestLocations(
        Location.objects.get(name='England'),
        Location.objects.get(name='Scotland')
    )


@pytest.fixture
def weather_data_sample(test_metrics, test_locations) -> List[Value]:
    values = []
    for year in range(2011, 2014):
        for month in range(1, 13):
            for location in test_locations:
                values.append(
                    Value.objects.create(
                        metric=test_metrics.Tmax,
                        location=location,
                        date=datetime.date(year, month, 1),
                        value=random.uniform(-10, 35)
                    )
                )
                values.append(
                    Value.objects.create(
                        metric=test_metrics.rainfall,
                        location=location,
                        date=datetime.date(year, month, 1),
                        value=random.uniform(0, 150)
                    )
                )
    yield values
