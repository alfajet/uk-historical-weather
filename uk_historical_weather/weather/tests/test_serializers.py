import json

import pytest
from django.http import HttpResponse
from django.test import Client
from django.urls import reverse
from rest_framework import status

from uk_historical_weather.weather.serializers import ValueSerializer

pytestmark = pytest.mark.django_db

client = Client()


def test_serialised_all_sample_values_check_equal_to_get_value_list(weather_data_sample):
    response = client.get(reverse('weather:value-list'))
    serializer = ValueSerializer(weather_data_sample, many=True)
    assert response.status_code == status.HTTP_200_OK
    assert response.data['results'], serializer.data


def test_filters_on_year_check_results_are_equal_to_filtered_sample(weather_data_sample):
    values_gte_2012 = [value for value in weather_data_sample if value.date.year >= 2012]
    response_gte_2012 = client.get(reverse('weather:value-list'), {'year_min': 2012})
    serializer_gte_2012 = ValueSerializer(values_gte_2012, many=True)
    assert response_gte_2012.status_code == status.HTTP_200_OK
    assert response_gte_2012.data['results'] == serializer_gte_2012.data


def test_filters_on_month_check_results_are_equal_to_filtered_sample(weather_data_sample):
    values_month_eq = [value for value in weather_data_sample if value.date.month == 6]
    response_month_eq = client.get(reverse('weather:value-list'), {'month': 6})
    serializer_month_eq = ValueSerializer(values_month_eq, many=True)
    assert response_month_eq.status_code == status.HTTP_200_OK
    assert response_month_eq.data['results'] == serializer_month_eq.data


def test_filters_on_location_check_results_are_equal_to_filtered_sample(weather_data_sample, test_locations):
    values_england = [value for value in weather_data_sample if value.location == test_locations.England]
    response_england = client.get(reverse('weather:value-list'), {'location': 'England'})
    serializer_england = ValueSerializer(values_england, many=True)
    assert response_england.status_code == status.HTTP_200_OK
    assert response_england.data['results'] == serializer_england.data


def test_filters_on_location_and_metrics_check_results_are_equal_to_filtered_sample(
    weather_data_sample, test_locations, test_metrics
):
    values_tmax_scotland = [
        value for value in weather_data_sample
        if value.location == test_locations.Scotland and value.metric == test_metrics.Tmax
    ]
    response_tmax_scotland = client.get(
        reverse('weather:value-list'),
        {'location': 'Scotland', 'metric': 'Tmax'})
    serializer_tmax_scotland = ValueSerializer(values_tmax_scotland, many=True)
    assert response_tmax_scotland.status_code == status.HTTP_200_OK
    assert response_tmax_scotland.data['results'] == serializer_tmax_scotland.data


def test_filters_on_metrics_lte__check_results_are_equal_to_filtered_sample(
    weather_data_sample, test_locations, test_metrics
):
    values_tmax_scotland_lte = [
        value for value in weather_data_sample
        if value.location == test_locations.Scotland and value.metric == test_metrics.Tmax and value.value <= 10
    ]

    response_tmax_scotland_lte = client.get(
        reverse('weather:value-list'),
        {'location': 'Scotland', 'metric': 'Tmax', 'value_max': 10})
    serializer_tmax_scotland_lte = ValueSerializer(values_tmax_scotland_lte, many=True)
    assert response_tmax_scotland_lte.status_code == status.HTTP_200_OK
    assert response_tmax_scotland_lte.data['results'] == serializer_tmax_scotland_lte.data


@pytest.fixture()
def valid_weather_input():
    yield {
        'year': 2020,
        'month': 1,
        'location': 'UK',
        'metric': 'Tmin',
        'value': 2.5,
    }


@pytest.fixture()
def invalid_location_input():
    yield {
        'year': 2020,
        'month': 1,
        'location': 'France',
        'metric': 'Tmin',
        'value': 2.5,
    }


@pytest.fixture()
def invalid_metric_input():
    yield {
        'year': 2020,
        'month': 1,
        'location': 'England',
        'metric': 'Dew Point',
        'value': 2.5,
    }


def get_weather_value_post_response(data) -> HttpResponse:
    return client.post(
        reverse('weather:value-list'),
        data=json.dumps(data),
        content_type='application/json'
    )


def test_post_valid_weather_value_check_success_status_code(valid_weather_input):
    response = get_weather_value_post_response(valid_weather_input)
    assert response.status_code == status.HTTP_201_CREATED


def test_post_invalid_location_value_check_response_returns_bad_request(invalid_location_input):
    response = get_weather_value_post_response(invalid_location_input)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_post_invalid_metric_value_check_response_returns_bad_request(invalid_metric_input):
    response = get_weather_value_post_response(invalid_metric_input)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
