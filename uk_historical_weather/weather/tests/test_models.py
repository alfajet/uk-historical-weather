import datetime

import pytest
from django.conf import settings
from django.db import IntegrityError

from uk_historical_weather.weather.models import Value, Location, Metric

pytestmark = pytest.mark.django_db

DEFAULT_DATE = datetime.date(2021, 1, 1)
DEFAULT_TEST_VALUE = 5.2
DEFAULT_LOCATIONS = getattr(settings, 'DEFAULT_LOCATIONS')
DEFAULT_METRICS = getattr(settings, 'DEFAULT_METRICS')


@pytest.fixture()
def value_obj(test_locations, test_metrics):
    yield Value.objects.create(
        metric=test_metrics.Tmax, location=test_locations.England, value=DEFAULT_TEST_VALUE, date=DEFAULT_DATE
    )


def test_db_initialised_with_expected_locations():
    assert [location.name for location in Location.objects.all()] == DEFAULT_LOCATIONS


def test_db_initialised_with_expected_metrics():
    assert [metric.name for metric in Metric.objects.all()] == DEFAULT_METRICS


def test_create_value_check_location_is_correct(value_obj, test_locations):
    assert value_obj.location == test_locations.England


def test_create_value_check_metric_is_correct(value_obj, test_metrics):
    assert value_obj.metric == test_metrics.Tmax


def test_create_value_check_str_contains_date_metric_location(value_obj):
    assert str(value_obj) == f"{DEFAULT_DATE.strftime('%Y-%m')}, metric=Tmax, location=England"


def test_create_value_check_repr_contains_classname_date_metric_location(value_obj):
    assert repr(value_obj) == f"Value({DEFAULT_DATE.strftime('%Y-%m')}, metric=Tmax, location=England)"


def test_create_twice_the_same_value_check_raise_integrity_error_with_unique_constraint_violation(value_obj):
    duplicate_value = Value(
        metric=value_obj.metric, location=value_obj.location, value=0, date=value_obj.date
    )
    with pytest.raises(IntegrityError) as excinfo:
        duplicate_value.save()
    assert f"Key (location_id, metric_id, date)=({value_obj.location_id}, " \
           f"{value_obj.metric_id}, {DEFAULT_DATE.isoformat()}) already exists." in str(excinfo.value)


def test_create_value_with_date_in_same_month_raise_integrity_error_with_unique_constraint_violation(value_obj):
    test_date = datetime.date(DEFAULT_DATE.year, DEFAULT_DATE.month, 10)
    duplicate_value = Value(
        metric=value_obj.metric, location=value_obj.location, value=0, date=test_date
    )
    with pytest.raises(IntegrityError) as excinfo:
        duplicate_value.save()
    assert f"Key (location_id, metric_id, date)=({value_obj.location_id}, " \
           f"{value_obj.metric_id}, {DEFAULT_DATE.isoformat()}) already exists." in str(excinfo.value)


def test_location_check_str_is_name(test_locations):
    assert str(test_locations.England) == "England"


def test_location_check_repr_has_classname_and_location_name(test_locations):
    assert repr(test_locations.England) == "Location(England)"


def test_create_location_check_slug_added_on_save():
    france = Location.objects.create(name='République Française')
    assert france.slug == "republique-francaise"


def test_metric_check_str_is_name(test_metrics):
    assert str(test_metrics.Tmax) == "Tmax"


def test_metric_check_repr_has_classname_and_metric_name(test_metrics):
    assert repr(test_metrics.Tmax) == "Metric(Tmax)"


def test_create_metric_check_slug_added_on_save():
    new_metric = Metric.objects.create(name="Dew Point")
    assert new_metric.slug == "dew-point"
