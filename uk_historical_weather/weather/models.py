import datetime

from django.db import models
from django.utils.text import slugify


class NamedModel(models.Model):
    """
    Base Abstract Model named objects.
    This generate str and repr as well as
    """

    name = models.CharField(unique=True, max_length=255, null=False, blank=False)
    slug = models.SlugField(unique=True, max_length=255, null=False, blank=False)

    def __str__(self):
        return self.name

    def __repr__(self):
        return f'{self.__class__.__name__}({self.__str__()})'

    def save(self, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super().save(**kwargs)

    class Meta:
        abstract = True


class Location(NamedModel):
    pass


class Metric(NamedModel):
    pass


class Value(models.Model):
    date = models.DateField(null=False, blank=False)
    value = models.FloatField(null=True)
    metric = models.ForeignKey(Metric, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)

    DATE_FORMAT = '%Y-%m'

    def save(self, **kwargs):
        """
        Before saving, we ensure that the day is set to one as records are monthly values
        """
        self.date = datetime.date(self.date.year, self.date.month, 1)
        super().save(**kwargs)

    def __str__(self):
        return f'{self.date.strftime(self.DATE_FORMAT)}, metric={self.metric}, location={self.location}'

    def __repr__(self):
        return f'{self.__class__.__name__}({self.__str__()})'

    class Meta:
        unique_together = ['location', 'metric', 'date']
