from typing import Type

from django.core.exceptions import ObjectDoesNotExist
from django_filters import rest_framework as filters
from rest_framework import mixins, serializers
from rest_framework.exceptions import ValidationError
from rest_framework.viewsets import GenericViewSet

from .models import Value, Location, Metric
from .serializers import ValueSerializer, ValuePostSerializer


class ValueFilter(filters.FilterSet):
    year_min = filters.NumberFilter(field_name='date__year', lookup_expr='gte')
    year_max = filters.NumberFilter(field_name='date__year', lookup_expr='lte')
    year = filters.NumberFilter(field_name='date__year',)
    month_min = filters.NumberFilter(field_name='date__month', lookup_expr='gte')
    month_max = filters.NumberFilter(field_name='date__month', lookup_expr='lte')
    month = filters.NumberFilter(field_name='date__month')
    value_min = filters.NumberFilter(field_name='value', lookup_expr='gte')
    value_max = filters.NumberFilter(field_name='value', lookup_expr='lte')
    location = filters.CharFilter(field_name='location__name')
    metric = filters.CharFilter(field_name='metric__name')

    class Meta:
        model = Value
        fields = 'year_min year_max year month_min month_max month value_min value_max location metric'.split(' ')


class ValueViewSet(mixins.ListModelMixin, mixins.CreateModelMixin, GenericViewSet):
    queryset = Value.objects.all()
    filterset_class = ValueFilter

    def get_serializer_class(self) -> Type[serializers.ModelSerializer]:
        if self.action == 'create':
            return ValuePostSerializer
        return ValueSerializer

    def create(self, request, *args, **kwargs):
        year = request.data.pop('year')
        month = request.data.pop('month')
        request.data['date'] = f'{year}-{month:02d}'
        try:
            request.data['location'] = Location.objects.get(name=request.data['location']).pk
        except ObjectDoesNotExist:
            raise ValidationError(f'Location {request.data["location"]} is not valid.')
        try:
            request.data['metric'] = Metric.objects.get(name=request.data['metric']).pk
        except ObjectDoesNotExist:
            raise ValidationError(f'Metric {request.data["metric"]} is not valid.')

        return super().create(request, *args, **kwargs)
