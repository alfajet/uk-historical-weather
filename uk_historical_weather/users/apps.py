from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "uk_historical_weather.users"
    verbose_name = _("Users")

    def ready(self):
        try:
            import uk_historical_weather.users.signals  # noqa F401
        except ImportError:
            pass
