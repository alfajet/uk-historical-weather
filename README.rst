UK Historical Weather
=====================

UK Historical Weather REST api

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style


:License: GPLv3


Settings
--------

Moved to settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Installation
------------

Prerequisites
^^^^^^^^^^^^^

* Python >= 3.6 (use of f-strings)
* postgresql: Other RDBMS may work but are currently untested

Installation steps
^^^^^^^^^^^^^^^^^^^

#. Clone this repository::

    $ git clone https://framagit.org/alfajet/uk-historical-weather.git
    $ cd uk_historical_weather


#. Create a virtual environment and source it. Example given with virtualenvwrapper::

    $ mkvirtualenv uk_historical_weather
    $ workon uk_historical_weather

#. Install required modules::

    $ pip install -r requirements/local.txt

#. Create a database::

    $ createdb ukweather


#. Migrate schema and initial data::


    $ python manage.py migrate


Note: if using a different db name, consider updating DATABASE_URL variable on config/settings/base.py or passing it as
a environment variable.


API Overview
-------------

The API allows to get and post weather values. Check the screencast to see it in action (GET only). Click to view:

.. image:: ./docs/assets/api.png
    :alt: Commands screencast
    :target: ./docs/assets/api_get_podcast.webm


Basic Commands
--------------

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.


Fetching and extracting weather data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The weather app comes with two custom commands, fetch and get to respectively retrieve and import weather data
from metoffice reports, and to export weather reports in the same format as the source.

Here are a few examples.

::

    $ python manage.py fetch --location England --metric Tmax  # Imports Tmax for England in the database
    $ python manage.py get --location England --metric Tmax  # Get Tmax for England printed in the standard output
    $ python manage.py get --location England --metric Tmax --max 10  # Get the first 10 elements
    $ python manage.py get --location England --metric Tmax --path ~/EnglandTmax.json  # Output results in the specified file name.

Screencast (click to view the video):

.. image:: ./docs/assets/commands.png
    :alt: Commands screencast
    :target: ./docs/assets/commands_fetch_get.webm

Testing
--------------

Type checks
^^^^^^^^^^^

Running type checks with mypy:

::

  $ mypy uk_historical_weather

Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage::

    $ pytest --cov --cov-config=.coveragerc

open htmlcov/index.html

Running tests
~~~~~~~~~~~~~

::

  $ pytest

.. Live reloading and Sass CSS compilation
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
..
.. Moved to `Live reloading and SASS compilation`_.
..
.. .. _`Live reloading and SASS compilation`: http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html





.. Deployment
.. ----------

..  The following details how to deploy this application.




