pytz  # https://github.com/stub42/pytz
python-slugify  # https://github.com/un33k/python-slugify
Pillow  # https://github.com/python-pillow/Pillow
argon2-cffi  # https://github.com/hynek/argon2_cffi
whitenoise  # https://github.com/evansd/whitenoise
redis # https://github.com/andymccurdy/redis-py

# Django
# ------------------------------------------------------------------------------
django>=3.2.0, < 3.3  # pyup: < 3.0  # https://www.djangoproject.com/
django-environ  # https://github.com/joke2k/django-environ
django-model-utils  # https://github.com/jazzband/django-model-utils
django-allauth  # https://github.com/pennersr/django-allauth
django-crispy-forms  # https://github.com/django-crispy-forms/django-crispy-forms
django-redis  # https://github.com/niwinz/django-redis

# Django REST Framework
djangorestframework>=3.12.0, < 3.13.0  # https://github.com/encode/django-rest-framework
django-filter
tqdm >=4.43, < 5.0  # https://github.com/tqdm/tqdm
